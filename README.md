simresist: simulation of the population dynamics of resistance and transmission of human helminth infections
============================================================================================================
This open-source software package was developed for and is described in the paper "Predicting the risk and speed of drug resistance emerging
in soil-transmitted helminths during preventive chemotherapy" by Coffeng et al. See Appendix A for the operating system and version numbers of the software and R packages that simresist was tested with.

Licence
=======
Please check the LICENCE file.

Citation
========
The package version used in the aforementioned paper (v1.1.1) can be cited as follows: "Coffeng LE (2023) simresist (v1.1.1): simulation of the population dynamics of resistance and transmission of human helminth infections in R. https://doi.org/10.5281/zenodo.10442358".

Installation instructions
=========================
(1) Install R, following the instructions for your operating system here: https://cran.r-project.org/

To install the simresist pacakge:

(2) Install R developer tools and the R package "dev.tools", following the instructions here: https://www.r-project.org/nosvn/pandoc/devtools.html

(3) Install the R package "remotes" to be able to access this Gitlab repository from within R. To do this, from within R, simply type (without the double quotes): "install.packages('remotes')"

(4) Install the R package "data.table", following the instructions here: https://www.rdocumentation.org/packages/data.table/

(5) Install the simresist package from within R with the following call from within R (without the double quotes): "remotes::install_gitlab('luccoffeng/simresist@master')"

Installation should only take a few moments (after completing downloading of the source code).

User instructions
=================
See Appendix A of the paper "Predicting the risk and speed of drug resistance
emerging in soil-transmitted helminths during preventive chemotherapy" by
Coffeng et al. The typical run time for a simulation highly depends on
user-defined parameter values; simulation time scales with the number of worms
in the simulation, which depends on the specified transmission conditions and
history of PC. However, given a pre-simulated dataset of baseline conditions
(e.g., available from https://doi.org/10.6084/m9.figshare.24521431), a 20-year
simulation takes between 10 seconds to up to one minute on a high-end laptop
(e.g., MacBook M1 Pro). Running simulations in parallel via the foreach package
is possible and recommended (see
https://gitlab.com/luccoffeng/simresist-nature-comms-sth-paper for examples).
