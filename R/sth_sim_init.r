# Initialise simulation --------------------------------------------------------
#' Initialise simulation
#'
#' \code{sim_init} initialises a list object containing the state of a host
#' population, worm population, environmental reservoir of infection, and
#' several auxiliary elements. Human demography will be warmed up for a
#' user-defined duration. Worm demography will not be completely warmed up,
#' meaning that within \code{simresist} the simulation will need to be warmed up
#' further.
#'
#' @param n_human_init Initial number of humans.
#' @param n_human_max Maximum number of humans.
#' @param n_worm_init Initial number of worms.
#' @param n_aux_cloud Number of auxiliary samples for administration of cloud
#'   traits.
#' @param trait_sd_gen Standard deviation among offspring of genetic component
#'   of quantitative trait for effect of drug treatment on worm.
#' @param p_SNP Initial frequency of the resistance-conferring allele.
#' @param human_warmup_duration Duration of warming up human demography (years).
#' @param human_warmup_steps Number of time steps per year for warming up human
#'   demography.
#' @param ... Arguments passed down via \code{...} in the \code{simresist}
#'   function.
#'
#' @export
sim_init <- function(n_human_init,
                     n_human_max,
                     n_worm_init,
                     n_aux_cloud,
                     trait_sd_gen,
                     p_SNP,
                     human_warmup_duration,
                     human_warmup_steps,
                     ...) {

  # Catch and adapt parameters for initiation and warmup
  param <- list(...)
  param <- within(param, {
    # Adjust time step for warmup of human demography
    fert <- fert_annual / human_warmup_steps
  })

  # Warm up human demography with 1-year stepsize
  human_warmup_duration <- human_warmup_duration * human_warmup_steps
  dt <- 1 / human_warmup_steps

  humans <- do.call(gen_human, c(n = n_human_init,
                                 id_start = 1,
                                 dt = dt,
                                 param))

  humans[age < age_death,  # avoid crash
         age := runif(.N, min = age, max = age_death)]

  for(i in 1:human_warmup_duration) {
    humans[, age := age + dt]
    do.call(update_human_fertility, c(x = list(humans),
                                      dt = dt,
                                      param))
    which_remove <- humans[age >= age_death, ID]
    if (humans[!(ID %in% which_remove), .N > n_human_max]) {
      which_remove <- c(which_remove,
                        do.call(reap,
                                c(x = list(humans[!(ID %in% which_remove)]),
                                  param)))
    }
    humans[ID %in% which_remove, dead := 1L]
    n_births <- humans[(!dead), rpois(1, sum(fertility, na.rm = TRUE))]
    if (n_births > 0) {
      humans <- rbindlist(list(humans[dead == 0],
                               do.call(gen_human,
                                       c(n = n_births,
                                         id_start = humans[, max(ID)] + 1L,
                                         dt = dt,
                                         param))))
    } else {
      if (length(which_remove > 0)) {
        humans <- humans[dead == 0]
      }
    }
  }
  do.call(update_human_fertility, c(x = list(humans), param))
  do.call(update_expo_contr_human, c(humans = list(humans),
                                     time = 0,
                                     param))

  # Create a cohort of newborn worms (assuming population at equilibrium),
  # meaning that standing genetic variation in population is twice the variation
  # among offspring.
  N_f <- rbinom(n = 1, size = n_worm_init, prob = .5)
  N_m <- n_worm_init - N_f

  males <- do.call(gen_worm, c(n = N_m,
                               trait_mu = 0,
                               trait_sd_gen = trait_sd_gen * sqrt(2),
                               female = FALSE,
                               dt = 0,
                               p_SNP = p_SNP,
                               param))
  females <- do.call(gen_worm, c(n = N_f,
                                 trait_mu = 0,
                                 trait_sd_gen = trait_sd_gen * sqrt(2),
                                 female = TRUE,
                                 dt = 0,
                                 p_SNP = p_SNP,
                                 param))
  worms <- rbindlist(list(females, males), fill = TRUE)
  worms[, age := runif(.N, min = age, max = age_death)]
  worms[age >= param$w_patent, patent := TRUE]
  if (sd(worms[, trait_gen]) > 0  & worms[, .N] > 1) {
    # Center and scale genetic traits to exactly match input values
    worms[, trait_gen := (trait_gen - mean(trait_gen)) *
            trait_sd_gen * sqrt(2) / sd(trait_gen)]
  }
  worms[, host := sample(x = humans[, ID],
                         size = .N,
                         replace = TRUE,
                         prob = humans[, rel_expo])]
  worms[, host_suitability := humans[match(host, ID), suitability]]

  # Update relative exposure and contribution to the environmental reservoir
  do.call(update_expo_contr_worm,
          c(humans = list(humans),
            worms = list(worms)))

  # Return result
  return(list(worms = worms,
              humans = humans,
              n_human_cum = humans[, max(ID)],
              cloud_reservoir = 0,
              cloud_trait = rnorm(n = n_aux_cloud,
                                  mean = 0,
                                  sd = trait_sd_gen * sqrt(2)),
              cloud_genotype = cbind(init_genotype(n = n_aux_cloud,
                                                   p_SNP = p_SNP),
                                     init_genotype(n = n_aux_cloud,
                                                   p_SNP = p_SNP))
              ))
}


### END OF CODE ### ------------------------------------------------------------
