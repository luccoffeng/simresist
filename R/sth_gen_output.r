# Generate simulation output ---------------------------------------------------
#' Generate simulation output
#'
#' Generate summary statistics of the current state of the simulation. When
#' adapting this function, make sure to adapt \code{aggregate_monitor} to deal
#' correctly with any potential changes in the output format.
#'
#' @param x Current state of the simulation
#' @param time Current time in the simulation.
#' @param age_output Vector of lower bounds for age categories for which output
#'   is generated. The lower bound is included in the age category, meaning that
#'   a vector \code{c(0, 2, 5, 15)} will result in output being generated for
#'   age groups 0-1.999, 2-4.999, 5-14.999, and 15 and above.
#' @param egg_prod_c Value to scale relative egg densities back to the scale of
#'   expected number of eggs in a single sample of weight \code{w_sample}.
#' @param n_sample Integer number of samples to be tested per individual.
#' @param k_sample Shape of the negative binomial distribution for
#'   overdispersion of repeated egg counts in an individual.
#' @param w_sample Weight of the sample (grams), used to convert egg counts to
#'   eggs per gram faeces (epg).
#' @param intens_cut Cut-offs for light, moderate, and heavy intensity infection
#'   (lower bounds) in terms of eggs per gram faeces (epg).
#' @param zeta Transmission rate, used to calculate force of infection (foi).
#' @param steps Integer number of discrete time steps per year in simulation.
#' @param ... Arguments passed down via \code{...} in the \code{simresist}
#'   function.
#'
#' @return Returns a \code{data.table} with various summary statistics,
#' following the following nomenclature and abbrevations:
#' \describe{
#'   \item{\code{n}}{Number.}
#'   \item{\code{a}}{Arithmetic mean.}
#'   \item{\code{var}}{Variance (based on denominator \code{n}, not \code{n-1}).}
#'   \item{\code{light}, \code{moderate}, \code{heavy}}{Categories for infection
#'   intensity based on cut-offs for eggs per gram faeces as defined by the
#'   \code{intens_cut} parameter in \code{param_worm_*}.}
#'   \item{\code{w, wf, wm, wpp}}{Worms: any, female (patent), male (patent),
#'   prepatent.}
#'   \item{\code{epg}}{Eggs per gram faeces.}
#'   \item{\code{age_cat}}{Lower bound of the host age category.}
#'   \item{\code{heteroz}, \code{homoz}}{Heterozygous and homozygous genotypes
#'   carrying the resistant allele.}
#'   \item{\code{a_drug_efficacy_true}}{Average drug efficacy in entire worm
#'   population.}
#'   \item{\code{trait_gen}, \code{trait_t}}{Quantitative trait for polygenic
#'   inheritance of drug resistance (genetic component and the temporal
#'   phenotypic trait).}
#'   \item{\code{foi}}{Force of infection acting on population (worms per person
#'   per year).}
#' }
#'
#' @importFrom boot logit
#' @importFrom boot inv.logit
#' @export
gen_output <- function(x,
                       time,
                       age_output,
                       egg_prod_c,
                       n_sample,
                       k_sample,
                       w_sample,
                       intens_cut,
                       zeta,
                       steps,
                       ...) {

  host_summary <-
    x$worms[(!dead) & age >= 0][x$humans, on = c(host = "ID"),
                                .("host_age" = i.age,
                                  # "host_sex" =i.sex,
                                  "host_exposure" = unique(rel_expo),

                                  "n_w" = .N,
                                  "n_wf" = sum(patent & sex == 1),
                                  "n_wm" = sum(patent & sex == 0),
                                  "n_wpp" = sum(!patent),
                                  "egg_dens_sample" = egg_prod_c * sum(eggs, na.rm = TRUE),

                                  "n_w_heteroz" = sum(geno_SNP == 1),
                                  "n_w_homoz" = sum(geno_SNP == 2),

                                  # pre-cursor to mean true drug efficacy
                                  "sum_drug_efficacy" = sum(inv.logit(pheno_worm - trait_t)),

                                  # pre-cursors for mean and variance estimates
                                  "trait_gen_sum" = sum(trait_gen),
                                  "trait_gen_sumsq" = sum(trait_gen^2),
                                  "trait_t_sum" = sum(trait_t),
                                  "trait_t_sumsq" = sum(trait_t^2)),
                                by = .EACHI]

  host_summary[, age_cat := cut(x = host_age,
                                breaks = c(age_output, Inf),
                                labels = age_output,
                                right = FALSE,
                                include.lowest = TRUE)]

  host_summary[, foi := steps * zeta * x$cloud_reservoir * host_exposure / .N]  # is same as:
  # host_summary[, foi := steps * zeta * x$cloud_reservoir *
  #                mean(host_exposure) * host_exposure / sum(host_exposure)]

  host_summary[, epg := sapply(egg_dens_sample,
                               function(y) mean(rnbinom(n = n_sample,
                                                        mu = y,
                                                        size = k_sample))) /
                 w_sample]
  host_summary[, intensity := cut(x = epg,
                                  breaks = c(intens_cut, Inf),
                                  include.lowest = TRUE,
                                  right = FALSE,
                                  labels = c("light",
                                             "moderate",
                                             "heavy"))]

  host_summary[!is.na(age_cat),  # drop humans that are scheduled to be born in the current time step
               .("time" = time,
                 "n_host" = .N,
                 "n_host_wfpos" = sum(n_wf > 0, na.rm = TRUE),
                 "n_host_eggpos" = sum(epg > 0, na.rm = TRUE),
                 "n_host_light" = sum(intensity == "light", na.rm = TRUE),
                 "n_host_moderate" = sum(intensity == "moderate", na.rm = TRUE),
                 "n_host_heavy" = sum(intensity == "heavy", na.rm = TRUE),
                 "foi" = mean(foi),

                 "n_w" = sum(n_w),
                 "n_wf" = sum(n_wf, na.rm = TRUE),
                 "n_wm" = sum(n_wm, na.rm = TRUE),
                 "n_wpp" = sum(n_wpp, na.rm = TRUE),

                 "a_epg_true" = sum(egg_dens_sample, na.rm = TRUE) /
                   w_sample / .N,
                 "a_epg_obs" = sum(epg, na.rm = TRUE) / .N,

                 "n_w_heteroz" = sum(n_w_heteroz, na.rm = TRUE),
                 "n_w_homoz" = sum(n_w_homoz, na.rm = TRUE),
                 "a_drug_efficacy_true" = sum(sum_drug_efficacy, na.rm = TRUE) / sum(n_w, na.rm = TRUE),

                 "a_trait_gen" = sum(trait_gen_sum, na.rm = TRUE) / sum(n_w, na.rm = TRUE),
                 "var_trait_gen" = (sum(trait_gen_sumsq, na.rm = TRUE) -
                                      (sum(trait_gen_sum, na.rm = TRUE)^2 /
                                         sum(n_w, na.rm = TRUE))) / sum(n_w, na.rm = TRUE),
                 "a_trait_t" = sum(trait_t_sum, na.rm = TRUE) / sum(n_wf + n_wm + n_wpp, na.rm = TRUE),
                 "var_trait_t" = (sum(trait_t_sumsq, na.rm = TRUE) -
                                    (sum(trait_t_sum, na.rm = TRUE)^2 /
                                       sum(n_w, na.rm = TRUE))) / sum(n_w, na.rm = TRUE)),
               by = age_cat]

}


# Aggregate simulation output over host age ------------------------------------
#' Aggregate simulation output over host age
#'
#' Summarise the age-specific simulation output.
#'
#' @param x A data.table containing age-specific output as generated by
#'   \code{gen_output}.
#' @param strata Optional character value to stratify by variables other than
#'   and in addition to time. Specify as single character value with variable
#'   names separated by a comma (no space after comma), e.g. \code{strata =
#'   "baseline_id,scen_id".}
#'
#' @return Returns a \code{data.table} with summary statistics as generated by
#'   \code{gen_output}, but aggregated over host age.
#'
#' @export
aggregate_monitor <- function (x, strata = "") {

  strata <- paste0("time,", strata)
  strata <- unlist(strsplit(strata, ","))

  x[, c(
    # Absolute count variables
    sapply(c("n_host", "n_host_wfpos", "n_host_eggpos",
             "n_host_light", "n_host_moderate", "n_host_heavy",
             "n_w", "n_wf", "n_wm", "n_wpp",
             "n_w_heteroz", "n_w_homoz"),
           function(i) sum(get(i), na.rm = TRUE),
           USE.NAMES = TRUE,
           simplify = FALSE),
    # Variables that have number of humans as denominator
    sapply(c("a_epg_true", "a_epg_obs", "foi"),
           function(i) weighted.mean(get(i), w = n_host, na.rm = TRUE),
           USE.NAMES = TRUE,
           simplify = FALSE),
    # Variables that have number of worms as denominator
    sapply(c("a_drug_efficacy_true", "a_trait_gen", "a_trait_t"),
           function(i) weighted.mean(get(i),
                                     w = n_w,
                                     na.rm = TRUE),
           USE.NAMES = TRUE,
           simplify = FALSE),
    # Variances
    "var_trait_gen" = pool_variance_exact(sigma2 = var_trait_gen,
                                          mu = a_trait_gen,
                                          n = n_w),
    "var_trait_t" = pool_variance_exact(sigma2 = var_trait_t,
                                        mu = a_trait_t,
                                        n = n_w)),
    by = strata]

}

### END OF CODE ###
