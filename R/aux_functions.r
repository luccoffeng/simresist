# Round of a vector of values such that the sum of rounded values is equal to
# the rounded sum of the original values
smart_round <- function(x, digits = 0) {
  up <- 10 ^ digits
  x <- x * up
  y <- floor(x)
  indices <- tail(order(x-y), round(sum(x)) - sum(y))
  y[indices] <- y[indices] + 1
  y / up
}


# Pool variance of non-overlapping subsets of a population given a vector of
# means, variances, and sample sizes for each subset. Variance estimates are
# assumed to be based on finite population approach (i.e. with "n" as
# denominator and not "n - 1").
pool_variance_exact <- function(sigma2, mu, n) {
  weighted.mean(x = mu^2 + sigma2, w = n, na.rm = TRUE) -
    weighted.mean(x = mu, w = n, na.rm = TRUE)^2
}

### END OF CODE ###
