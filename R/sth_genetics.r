# Initialise genotype ---------------------------------------------------------
#' Initialise genotype
#'
#' Create a random set of genotypes assuming Hardy-Weinberg (HW) equilibrium.
#'
#' @param n Number of genotypes to create.
#' @param p_SNP Allelle frequency
#' @param exact Logical flag indicating whether the frequency of the randomly
#'   generated genotypes should match the specified frequency as closely as
#'   possible or not (i.e., allowing stochastic variation).
#' @param ... Arguments passed down via \code{...} in the \code{simresist}
#'   function.
#'
#' @return An integer vector of length \code{n} with values \code{0} (homozygous
#'   wildtype), \code{1} (heterozygous), and \code{2} (homozygous resistant).
#'
#' @export
init_genotype <- function(n,
                          p_SNP,
                          exact = FALSE,
                          ...) {

  # Genotype frequencies in Hardy-Weinberg equilibrium
  p_HW <- c("aa" = (1 - p_SNP)^2,
            "aA" = (1 - p_SNP) * p_SNP * 2,
            "AA" = (p_SNP)^2)

  if (exact == FALSE) {

    sample(x = 0:2, size = n, replace = TRUE, prob = p_HW)

  } else {

    freqs <- smart_round(p_HW * n, digits = 0)
    sample(x = unlist(mapply(rep, x = 0:2, times = freqs)),
           size = n,
           replace = FALSE,
           prob = NULL)

  }

}


# Generate offspring genotypes -------------------------------------------------
#' Generate offspring genotypes
#'
#' Generate a population of offspring genotypes.
#'
#' @param genotype_distr A matrix with n rows and 3 columns with in each row the
#'   probabilities of producing each of the three different genotypes ("aa",
#'   "Aa", "AA"), which should sum to one (no check is performed!).
#'
#' @return An integer vector of length n containing offspring genotypes
#'   (integer) representing the number of resistant alleles on a single locus
#'   (0L, 1L, or 2L).
#'
#' @export
gen_offspring_genotype <- function(genotype_distr) {

  apply(genotype_distr, 1, function(p) sample(x = 0:2, size = 1, prob = p))

}


# Generate a genotype distribution ---------------------------------------------
#' Generate a genotype distribution
#'
#' Generate genotype distributions for offspring given the number of
#' resistance-conferring alleles each parents in a parent pair has (0L, 1L, or
#' 2L).
#'
#' @param x A matrix with 2 columns with paternal and maternal worm genotypes.
#'
#' @return A matrix with 3 columns and a number of rows equal to the number of
#'   rows of x. Each row contains the probabilities of producing each of the
#'   three different genotypes ("aa", "Aa", "AA"), which sum to one.
#'
#' @export
gen_genotype_distr <- function(x) {

  p <- x / 2
  q <- 1 - p

  cbind(q[, 1] * q[, 2],
        p[, 1] * q[, 2] + q[, 1] * p[, 2],
        p[, 1] * p[, 2])

}


# Reset all worm genetics to match input parameter values ----------------------
#' Reset all worm genetics to match input parameter values
#'
#' This function resamples genotypes and trait values such that their
#' distribution in the worm population exactly matches the input parameters.
#' Useful when a simulated timeline needs to be conditioned on an exact set of
#' genetic starting conditions (which are assumed to be in equilibrium). Note
#' that although the simulated SNP frequencies and quantitative trait
#' distributions will be guaranteed to be as close as possible to the specified
#' values, the survey output on these frequencies and distributions may still
#' be slightly different because genotypes and traits are also assigned to worms
#' that are about to be introduced in the next time step (and which are not yet
#' counted in the survey).
#'
#' @param x State object (list) containing the state of human and worm
#'   populations.
#' @param n_aux_cloud Number of infective particles to simulate in
#'   environmental reservoir (cloud) to approximate the mixture distribution of
#'   traits among worm eggs.
#' @param pheno_kill A vector with three probabilities that a worm is
#'   killed by the drug given each of the three genotypes related to monogenic
#'   drug resistance (in the order "aa", "aA", AA").
#' @param pheno_fitness A vector with three scalars (>0) representing the
#'   relative fitness of female worms in terms of relative egg productivity
#'   (default value of 1 indicates no fitness loss) for the three genotypes
#'   related to monogenic drug resistance (in the order "aa", "aA", AA").
#' @param trait_sd_gen Standard deviation among offspring of genetic component
#'   of quantitative trait for effect of drug treatment on worm.
#' @param trait_sd_env Standard deviation among offspring of non-genetic and
#'   non-temporal (environmental) component of quantitative trait for effect of
#'   drug treatment on worm.
#' @param trait_sd_t Standard deviation among offspring of non-genetic and
#'   temporal component of quantitative trait for effect of drug treatment on
#'   worm.
#' @param p_SNP Initial frequency of the resistance-conferring allele.
#' @param ... Arguments passed down via \code{...} in the \code{simresist}
#'   function.
#'
#' @return An updated state object with reset genetic values.
#'
#' @export
reset_genetics <- function(x,
                           n_aux_cloud,
                           pheno_kill,
                           pheno_fitness,
                           trait_sd_gen,
                           trait_sd_env,
                           trait_sd_t,
                           p_SNP,
                           ...) {

  x$worms[, geno_SNP := init_genotype(n = .N, p_SNP = p_SNP, exact = TRUE)]

  x$worms[, c("pheno_worm", "fitness") :=
            as.data.table(t(sapply(geno_SNP, function(a) {
              c(pheno_kill[(a + 1L)],
                pheno_fitness[(a + 1L)])})))]

  x$worms[, trait_gen := rnorm(n = .N, mean = 0, sd = sqrt(2) * trait_sd_gen)]
  if(x$worms[, sd(trait_gen)] > 0 & x$worms[, .N] > 1) {
    # Enforce mean and sd of genetic trait component to be exactly as specified
    x$worms[, trait_gen := (trait_gen - mean(trait_gen)) *
              sqrt(2) * trait_sd_gen / sd(trait_gen)]
  }
  x$worms[, trait_env := rnorm(n = .N, mean = trait_gen, sd = trait_sd_env)]
  x$worms[, trait_t := rnorm(n = .N, mean = trait_env, sd = trait_sd_t)]

  x$cloud_trait <- rnorm(n = n_aux_cloud,
                         mean = 0,
                         sd = sqrt(2) * trait_sd_gen)
  if(sd(x$cloud_trait) > 0) {
    # Enforce mean and sd of genetic trait component to be exactly as specified
    x$cloud_trait <- with(x, {
      (cloud_trait - mean(cloud_trait)) * sqrt(2) *
        trait_sd_gen / sd(cloud_trait)
    })
  }
  x$cloud_genotype <-
    cbind(init_genotype(n = n_aux_cloud, p_SNP = p_SNP, exact = TRUE),
          init_genotype(n = n_aux_cloud, p_SNP = p_SNP, exact = TRUE))

  return(x)

}


### END OF CODE ###

