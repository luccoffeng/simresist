# Generate human agents --------------------------------------------------------
#' Generate human agents
#'
#' Generate human agents. Argument values can be passed down via \code{...} in
#' the \code{simresist} function.
#'
#' @param n Number of new humans to be generated
#' @param id_start ID number for the first of the newly created humans.
#' @param age_table Ages to interpolate cumulative survival between (vector of
#'   equal lenght as surv_cum).
#' @param surv_cum Cumulative survival at ages in age_table (vector of equal
#'   lenght as age_table).
#' @param contrib_k Shape and rate parameter of gamma distribution (with mean
#'   1.0) for inter-individual variation in exposure and contribution to
#'   transmission. Plausible values from lit: 0.3-0.6 (hookworm species),
#'   0.1-0.65 (Trichuris trichiura), and 0.4-0.8 (Ascaris lumbricoides; see also
#'   Table 1 in [Anderson et al 2013 PLoS
#'   NTDs](https://doi.org/10.1371/journal.pntd.0002027)).
#' @param suitable_k Shape and rate parameter of gamma distribution (with mean
#'   1.0) for host suitability for worms to produce eggs in.
#' @param dt Time step size in simulation, used to randomly schedule births
#'   across the period spanned by a single time step.
#' @param ... Arguments passed down via \code{...} in the \code{simresist}
#'   function.
#'
#' @return Returns a \code{data.table} of human hosts.
#'
#' @export
gen_human <- function(n,
                      id_start,
                      age_table,
                      surv_cum,
                      contrib_k,
                      suitable_k,
                      dt,
                      ...) {

  data.table(ID = id_start:(id_start + n - 1L),
             sex = rbinom(n = n, size = 1, prob = 0.5),
             age = -runif(n, min = 0, max = dt),
             age_death = approx(x = surv_cum,
                                y = age_table,
                                xout = runif(n),
                                method = "linear")$y,
             fertility = 0,
             suitability = if (suitable_k == Inf) {
               1
             } else {
               rgamma(n = n, shape = suitable_k, rate = suitable_k)
             },
             rel_contr_expo_i = rgamma(n = n,
                                       shape = contrib_k,
                                       rate = contrib_k),
             rel_contr_age = 0,
             rel_expo_age = 0,
             rel_contr = 0,
             rel_expo = 0,
             expo_co = 1,       # effect of WASH / bednets if compliant
             contrib_co = 1,    # effect of WASH / bednets if compliant
             co_i = rnorm(n = n, mean = 0, sd = 1),  # MDA compliance factor
             ec_i = runif(n = n, min = 0, max = 1),  # WASH / bednet compliance factor
             dead = 0L)
}


# Generate worm agents ---------------------------------------------------------
#' Generate worm agents
#'
#' Generate worm agents. Argument values can be passed down via \code{...} in
#' the \code{simresist} function.
#'
#' @param n Number of new worms to be created.
#' @param w_lifespan_k Shape of lifespan distribution (Weibull distribution).
#' @param w_lifespan_scale Scale of lifespan distribution (Weibull distribution).
#' @param w_patent Age at which worms become patent.
#' @param trait_mu Vector of mean parent trait values.
#' @param trait_sd_gen Standard deviation among offspring of genetic component
#'   of quantitative trait for effect of drug treatment on worm.
#' @param trait_sd_env Standard deviation among offspring of non-genetic and
#'   non-temporal (environmental) component of quantitative trait for effect of
#'   drug treatment on worm.
#' @param trait_sd_t Standard deviation among offspring of non-genetic and
#'   temporal component of quantitative trait for effect of drug treatment on
#'   worm.
#' @param p_SNP Initial frequency of the resistance-conferring allele.
#' @param genotypes_parent optional matrix with 2 columns with paternal and
#'   maternal worm genotypes and a number of rows (i.e. male-female worm pairs)
#'   equal to n (this is not checked!). If not specified, random genotypes are
#'   generated based on \code{p_SNP}, which is passed down via \code{...} in
#'   \code{simresist}.
#' @param pheno_kill Vector of three phenotypes for the log-odds that a worm
#'   is killed depending on the number of resistant allelles a worm has (first
#'   position = zero allelles, second = one allelle, third = two allelles).
#' @param pheno_fitness A vector with three scalars (>0) representing the
#'   relative fitness of female worms in terms of relative egg productivity
#'   (default value of 1 indicates no fitness loss) for the three genotypes
#'   related to monogenic drug resistance (in the order "aa", "aA", AA").
#' @param female Logical flag indicating whether female worms need to be
#'   generated.
#' @param dt Time step size in simulation, used to randomly schedule births
#'   across the period spanned by a single time step.
#' @param ... Arguments passed down via \code{...} in the \code{simresist}
#'   function.
#'
#' @return Returns a \code{data.table} of worms.
#'
#' @export
gen_worm <- function(n,
                     w_lifespan_k,
                     w_lifespan_scale,
                     w_patent,
                     trait_mu,
                     trait_sd_gen,
                     trait_sd_env,
                     trait_sd_t,
                     p_SNP = NULL,
                     genotypes_parent = NULL,
                     pheno_kill,
                     pheno_fitness,
                     female,
                     dt,
                     ...) {
  if (n > 0) {

    # Create new worms
    worms <- data.table(
      age = -runif(n, min = 0, max = dt),
      patent = FALSE,
      age_death = rweibull(n = n, shape = w_lifespan_k,
                           scale = w_lifespan_scale),
      trait_gen = rnorm(n = n, mean = trait_mu, sd = trait_sd_gen),
      trait_env = as.numeric(NA),
      trait_t = as.numeric(NA),
      dead = 0L,
      host_suitability = as.numeric(NA),
      rel_contr = as.numeric(NA)
    )

    worms[, trait_env := rnorm(n = n, mean = trait_gen, sd = trait_sd_env)]
    worms[, trait_t := rnorm(n = n, mean = trait_env, sd = trait_sd_t)]

    if (is.null(genotypes_parent)) {
      param <- list(...)
      worms[, geno_SNP := init_genotype(n = .N, p_SNP = p_SNP)]
    } else {
      worms[, geno_SNP :=
              gen_offspring_genotype(gen_genotype_distr(genotypes_parent))]
    }

    worms[, c("pheno_worm", "fitness") :=
            as.data.table(t(sapply(geno_SNP, function(a) {
              c(pheno_kill[(a + 1L)],
                pheno_fitness[(a + 1L)])})))]

    if(female) {
      worms[, sex := 1L]
      worms[, insem := FALSE]

      # Add column for egg output
      worms[, eggs := 0]

      # Add auxiliary columns for larval traits
      worms[, egg_trait := as.numeric(NA)]
      worms[, geno_SNP_father := as.integer(NA)]

    } else {
      worms[, sex := 0L]
    }

    return(worms)
  } else {
    return(NULL)
  }
}


### END OF CODE ###

